<?php

/*
 Template Name: Landing Page
 */

 ?>
<?php get_header(); ?>

<?php
global $post;

$p_id = get_the_ID();

///////// Biography /////////

$biography_1 = get_field('biography_1',get_the_ID());
$biography_2 = get_field('biography_2',get_the_ID());

////////////////////////////////////

//////////////// Team //////////////

$args = array( 'numberposts' =>-1, 'post_type'=>'team', 'suppress_filters' => 0, 'orderby' => 'date','order' => 'ASC');
$team = get_posts( $args );

foreach ( $team as $post ) :   setup_postdata( $post );

$title = get_the_title();
$role = get_field('role',get_the_ID());
$desc = get_field('description',get_the_ID());


$team_array[] = array('title'=> $title, 'role'=> $role, 'desc'=> $desc);

endforeach;
wp_reset_postdata();

////////////////////////////////////

//////////////// News //////////////

$args = array( 'numberposts' =>-1, 'post_type'=>'news', 'suppress_filters' => 0, 'orderby' => 'date','order' => 'ASC');
$news = get_posts( $args );


foreach ( $news as $post ) :   setup_postdata( $post );

$title_1 = get_field('title_1',get_the_ID());
$title_2 = get_field('title_2',get_the_ID());
$title_3 = get_field('title_3',get_the_ID());
$desc_1 = get_field('desc_1',get_the_ID());
$desc_2 = get_field('desc_2',get_the_ID());
$column_1 = get_field('column_1',get_the_ID());
$column_2 = get_field('column_2',get_the_ID());
$month = get_field('month',get_the_ID());
$day = get_field('day',get_the_ID());
$year = get_field('year',get_the_ID());


$news_array[$year][] = array('title_1'=> $title_1, 'title_2'=> $title_2, 'title_3'=> $title_3, 'desc_1'=> $desc_1
  , 'desc_2'=> $desc_2, 'column_1'=> $column_1, 'column_2'=> $column_2, 'month'=> $month, 'day'=> $day, 'year'=> $year);

endforeach;
wp_reset_postdata();

// print_r($news_array);die;
////////////////////////////////////

//////////////// Awards //////////////

$args = array( 'numberposts' =>-1, 'post_type'=>'awards', 'suppress_filters' => 0, 'orderby' => 'date','order' => 'ASC');
$awards = get_posts( $args );

// print_r($awards);die;


foreach ( $awards as $post ) :   setup_postdata( $post );

$title = get_the_title();
$year = get_field('year',get_the_ID());
$special = get_field('special',get_the_ID());


$awards_array[$year][] = array('title'=> $title, 'year'=> $year, 'special'=> $special);

endforeach;
wp_reset_postdata();

ksort($awards_array);

// print_r($awards_array);die;
////////////////////////////////////

//////////////// Gallery //////////////

$gallery = get_field('gallery',get_the_ID());
$gallery = $gallery[0]['ngg_id'];

global $nggdb;
$gallery = $nggdb->get_gallery ($gallery, 'sortorder', 'ASC', true, 0, 0);

foreach ($gallery as $key => $image) {
  
  $gallery_images[] = $image->imageURL;
}

////////////////////////////////////

///////// Contact Us /////////

$contact_us_logo = get_field('contact_us_logo',get_the_ID());
$contact_us_text = get_field('contact_us_text',get_the_ID());

////////////////////////////////////

///////// Footer /////////

$instagram_icon = get_field('instagram_icon',get_the_ID());
$instagram_link = get_field('instagram_link',get_the_ID());

$facebook_icon = get_field('facebook_icon',get_the_ID());
$facebook_link = get_field('facebook_link',get_the_ID());

$twitter_icon = get_field('twitter_icon',get_the_ID());
$twitter_link = get_field('twitter_link',get_the_ID());

$footer_text = get_field('footer_text',get_the_ID());
$sponser_1 = get_field('sponser_1',get_the_ID());
$sponser_2 = get_field('sponser_2',get_the_ID());


////////////////////////////////////

// print_r($gallery_images);die;

?>

<link href="style_home.css" rel="stylesheet" media="screen">
<link href="assets/css/animate.css" rel="stylesheet" media="screen">


<title>Nour El Sherbini Official Website</title>
<div class="overlay" id="loading">
     <img class="spinner_logo animated infinite rubberBand" src="assets/img/n_logo.png">
  </div>
<div class="main_container">
  <nav class="navbar navbar-default navbar-fixed-top mobile_nav">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand mobile_links" href="#home" >
              <img class="n_logo" src="assets/img/n_logo.png">
            </a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="#bio" slideID="bio" class="menu_spans">Biography</a></li>
              <li><a href="#news" slideID="news" class="menu_spans">News</a></li>
              <li><a href="#awards" slideID="awards" class="menu_spans">Awards</a></li>
              <li><a href="#team" slideID="team" class="menu_spans">Team Sherbini</a></li>
              <li><a href="#photos" slideID="photos" class="menu_spans">Photos</a></li>
              <li><a href="#social" slideID="social" class="menu_spans">Social Media</a></li>
              <li><a href="#contact" slideID="contact" class="menu_spans">Contacts</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
  <div class="header">
    <img class="n_logo" src="assets/img/n_logo.png">
    <span slideID="bio" class="menu_spans">Biography</span>
    <span slideID="news" class="menu_spans">News</span>
    <span slideID="awards" class="menu_spans">Awards</span>
    <span slideID="team" class="menu_spans">Team Sherbini</span>
    <span slideID="photos" class="menu_spans">Photos</span>
    <span slideID="social" class="menu_spans">Social Media</span>
    <span slideID="contact" class="menu_spans">Contacts</span>
  </div>
  <div class="top_div" id="home">
    <img src="assets/img/main_bg.jpg">
  </div>
  <div class="bot_div">
    <div class="bot_rows" id="bio" slideID="bio">
      <div class="bio_row bot_rows_1" slideID="bio">
        <span>Biography</span>
        <img class="signs plus_sign plus_bio" slideID="bio" src="assets/img/plus_sign.png">
        <img class="signs minus_sign" src="assets/img/minus_sign.png">
      </div>
      <div class="bot_cont_div container bio_text">
        <div class="bio_text_div_left col-sm-6">
          <?= $biography_1; ?>
        </div>
        <div class="bio_text_div_right col-sm-6">
          <?= $biography_2; ?>
        </div>
      </div>
    </div>
    <div class="bot_rows" id="news" slideID="news" newsID='1'>
      <div class="bio_row bot_rows_1 container" slideID="news" newsID='1'>
        <span>News</span>
        <img class="signs plus_sign plus_news" slideID="news" newsID='1' src="assets/img/plus_sign.png">
        <img class="signs minus_sign" newsID='1' src="assets/img/minus_sign.png">
      </div>
      <div class="year_column col-sm-1">
        </div>
      <div class="bot_cont_div news_row">
        <?php
        foreach ($news_array as $key => $value) {
          
          foreach ($value as $key2 => $value2) {

            $old_year = '';

            if($key2 == 0)
            $old_year = $value2['year'];  
            
            ?>
            <div class="news_single_div">
              <div class="news_row_title">
                <span class="year_span"><?= $old_year; ?></span>
                <span class="news_span_1"><?=$value2['title_1'];?> <span class="news_span_3"><?=$value2['title_2'];?></span> <span class="news_span_5"><?=$value2['title_3'];?></span></span>
                <span class="news_span_2"><?=$value2['month'];?> <span class="news_span_3"><?=$value2['day'];?>, </span><span class="news_span_4"><?=$value2['year'];?>.</span></span>
              </div>
              <div class="news_row_cont container">
                <?php
                if(empty($value2['column_1']))
                {
                  ?>
                  <div class="news_row_desc_1 col-sm-6">
                    <?=$value2['desc_1'];?>
                  </div>
                  <?php

                  if(!empty($value2['desc_2']))
                  {
                    ?>
                    <span class="news_more_btn"><img src="assets/img/arrow.png">more...</span>
                    <div class="news_row_desc_2 col-sm-6">
                      <?=$value2['desc_2'];?>
                    </div>
                    <?php
                  }
                }
                else
                {
                  ?>
                  <div class="col_1 col-sm-6">
                    <?=$value2['column_1'];?>
                  </div>
                  <div class="col_2 col-sm-6">
                    <?=$value2['column_2'];?>
                  </div>
                  <?php
                }
                ?>
              </div>
            </div>
            <?php
          }
        }
        ?>
      </div>
    </div>
    <div class="bot_rows" id="awards" slideID="awards" awardsID='1'>
      <div class="bio_row bot_rows_1" slideID="awards" awardsID='1'>
        <span>Awards</span>
        <img class="signs plus_sign plus_awards" slideID="awards" awardsID='1' src="assets/img/plus_sign.png">
        <img class="signs minus_sign" awardsID='1' src="assets/img/minus_sign.png">
      </div>
      <div class="year_column_2 col-sm-1">
        </div>
      <div class="bot_cont_div awards_row">
        <?php
        foreach ($awards_array as $key => $value) {
          ?>
          <div class="awards_single_div">
          <?php
          foreach ($value as $key2 => $value2) {

            $old_year = '';

            if($key2 == 0)
            $old_year = $value2['year'];  

            $awards_special = '';
            if($value2['special'] == 'Yes')
            $awards_special = 'awards_special';  
            
            ?>
              <span class="year_span_2"><?= $old_year; ?></span>
              <span class="awards_span <?= $awards_special; ?>"><?= $value2['title']; ?></span>
            <?php
          }

          ?>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
    <div class="bot_rows" id="team" slideID="team">
      <div class="bio_row bot_rows_1" slideID="team">
        <span>Team Sherbini</span>
        <img class="signs plus_sign plus_team" slideID="team" src="assets/img/plus_sign.png">
        <img class="signs minus_sign" src="assets/img/minus_sign.png">
      </div>
      <div class="bot_cont_div container team_text">
        <div class="team_text_div_main col-sm-6">
          <?php
          foreach ($team_array as $key => $value) {
            
            ?>
            <div class="team_text_div_inner">
              <span class="team_span_1"><?= $value['title']; ?></span>
              <span class="team_span_2"><?= $value['role']; ?></span>
              <?= $value['desc']; ?>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
    <div class="bot_rows" id="photos" slideID="photos">
      <div class="bio_row bot_rows_1" slideID="photos">
        <span>Photos</span>
        <img class="signs plus_sign plus_photos" slideID="photos" src="assets/img/plus_sign.png">
        <img class="signs minus_sign" src="assets/img/minus_sign.png">
      </div>
      <div class="bot_cont_div gallery_cont_div container">
        <?php
        foreach ($gallery_images as $key => $value) {
          ?>
          <div class="gallery_image_div col-sm-4">
            <img class="gallery_image" src="<?= $value; ?>">
          </div>
          <?php
        }
        ?>
      </div>
    </div>
    <div class="bot_rows" id="social" slideID="social">
      <div class="bio_row bot_rows_1" slideID="social">
        <span>Social Media</span>
        <img class="signs plus_sign plus_social" slideID="social" src="assets/img/plus_sign.png">
        <img class="signs minus_sign" src="assets/img/minus_sign.png">
      </div>
      <div class="bot_cont_div container">
        <div class="fb_div col-sm-4">
          <?php 
          echo do_shortcode('[widget id="facebookpagewidget-2"]'); 
          // echo do_shortcode('[widget id="facebook_master_widget_basic-2"]'); 
          
          // echo do_shortcode("[socialfeed id='45']"); 
          ?>
        </div>
      </div>
        <div class="twitter_div col-sm-4">
          <div class="tweet_btn_div">
            <div class="tweet_btn_div_1 col-sm-6">
              <span class="twitter_span">@noursherbini</span>
            </div>
            <div class="tweet_btn_div_2 col-sm-6">
              <script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
             <a href="http://twitter.com/share" class="tweet_btn twitter-share-button"
                data-url="http://www.nourelsherbini.com/"
                data-text="Check out this site"
                data-related="Nour el Sherbini"
                data-count="vertical">Tweet</a>
            </div>
          </div>
          <div>
            <a href="https://twitter.com/noursherbini" class="follow_btn twitter-follow-button" data-show-count="false">Follow @noursherbini</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
          </div>
          <?php 
          echo do_shortcode('[widget id="qrokes-twitterwidget-2"]'); 
          ?>
        </div>
      </div>
    </div>
    <div class="bot_rows" id="contact" slideID="contact">
      <div class="bio_row bot_rows_1" slideID="contact">
        <span>Contacts</span>
        <img class="signs plus_sign plus_contact" slideID="contact" src="assets/img/plus_sign.png">
        <img class="signs minus_sign" src="assets/img/minus_sign.png">
      </div>
      <div class="bot_cont_div container">
        <div class="contact_us_text_div col-sm-4">
          <img class="contact_us_image" src="<?= $contact_us_logo; ?>">
          <?= $contact_us_text; ?>
        </div>
        <div class="col-sm-2">
        </div>
        <form class="form-horizontal form_submit" formID="1">
          <div class="form_div col-sm-6">
            <span>Your email:</span>
            <div class="form-group f_2">
              <div class="form_div_inner col-sm-12">
                <input inputID="2" type="text" class="form-control form_inputs col-sm-10" id="inputEmail3" name="email" required>
              </div>
            </div>
            <span>Your message:</span>
            <div class="form-group f_2">
              <div class="form_div_inner col-sm-12">
                <textarea class="form-control form_inputs col-sm-10" rows="3" name="message" inputID="1" required></textarea>
                <button type="submit" class="send_btn btn btn-default">send</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="white_strip"></div>
  <div class="footer container">
      <div class="footer_left_div col-sm-5">
        <div>
          <a href="<?= $instagram_link; ?>" target="_blank">
            <img class="footer_social" src="<?= $instagram_icon; ?>">
          </a>
          <a href="<?= $facebook_link; ?>" target="_blank">
            <img class="footer_social" src="<?= $facebook_icon; ?>">
          </a>
          <a href="<?= $twitter_link; ?>" target="_blank">
            <img class="footer_social" src="<?= $twitter_icon; ?>">
          </a>
        </div>
        <div>
          <span class="footer_span_1"><?= $footer_text; ?></span>
          <span class="footer_span_2"><?php echo date("Y"); ?> </span> <span class="footer_span_3"> All Rights Reserved</span>
        </div>
      </div>
      <div class="col-sm-3">
      </div>
      <div class="footer_right_div col-sm-4">
        <span class="footer_right_span">Sponsors</span>
        <img class="footer_right_img"src="<?= $sponser_1; ?>">
        <img class="footer_right_img" src="<?= $sponser_2; ?>">
      </div>
    </div>
</div>

<button class="md-trigger md_trigger_sending" data-modal="modal-2"></button>
<button class="md-trigger md_trigger_success" data-modal="modal-3"></button>

<div class="md-modal md-effect-2" id="modal-2">
  <div class="md-content">
    <h3><img src="assets/img/spinner.gif"/>Sending</h3>
  </div>
  <button class="md-close"></button>
</div>
<div class="md-modal md-effect-22" id="modal-3">
  <div class="md-content">
    <h3>Message Sent!</h3>
  </div>
  <button class="md-close"></button>
</div>

<div class="md-overlay"></div>
<link rel="stylesheet" type="text/css" href="assets/modal/component.css" />
<script src="assets/modal/modernizr.custom.js"></script>
<script src="assets/modal/classie.js"></script>
<script src="assets/modal/modalEffects.js"></script>


<?php get_footer(); ?>

<script>


$( document ).ready(function() {

  $('.spinner_logo').removeClass('infinite');
  $('.spinner_logo').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', doSomething);

  function doSomething()
  {
    $('.spinner_logo').addClass('animated pulse');
    $('.spinner_logo').removeClass('rubberBand');
    $('.spinner_logo').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', doSomething2);
  }

  function doSomething2()
  {
    $('.spinner_logo').addClass('animated rubberBand');
    $('.spinner_logo').removeClass('pulse');
    $('.spinner_logo').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', doSomething);
  }

  var windowWidth = parseInt($(window).width());

  if(windowWidth < 769)
  $('.year_column') .css('top', '83px');
  else
  $('.year_column') .css('top', '90px');  

  var h1 = parseInt($('.news_row').css('height'));
  h1 = h1 - 50;
  h1 = h1 + 'px';
  $('.year_column') .css('height', h1);

  var h2 = parseInt($('.awards_row').css('height'));
  h2 = h2 - 50;
  h2 = h2 + 'px';

  $('.year_column_2') .css('height', h2);

  
  $('.n_logo').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    var post = $('#home');
    var position = post.position().top - $(window).scrollTop();
    $('html, body').stop().animate({
        'scrollTop': post.offset().top
    }, 900, 'swing', function () {

      $('.plus_sign').slideDown();
      $('.minus_sign').hide();
      

      $('.bot_cont_div').hide();

      $('.year_column').hide();
      $('.year_column_2').hide();
      $('.active_sign').removeClass('active_sign');
        
    });

    if(windowWidth < 769)
    {
      new_height = parseInt($('.navbar-header').css('height')); 
      var flag1 = $('.mobile_nav').is(':visible');
      var flag2 = $('.mobile_nav .navbar-collapse').is(':visible');

      if(flag1 && flag2)
      $('.navbar-toggle').trigger('click');
    }

  });

  $('.menu_spans').on('click',function (e) {

    e.preventDefault();
    var slideID = $(this).attr('slideID');
    var post = $('.plus_'+slideID).trigger('click');
  });

   $('.mobile_links').on('click',function (e) {

    e.preventDefault();

  });

  
  $('.bot_rows_1').on('click',function (e) {


    e.preventDefault();

    {
      $('.active_sign').not(this).parent().find('.plus_sign').slideDown();
      $('.active_sign').not(this).parent().find('.minus_sign').hide();
      

      $('.active_sign').not(this).parent().find('.bot_cont_div').hide();

      $('.active_sign').not(this).parent().find('.year_column').hide();
      $('.active_sign').not(this).parent().find('.year_column_2').hide();
    }


    $(this).addClass('active_sign');

    var newsID = $(this).attr('newsID');
    var awardsID = $(this).attr('awardsID');

    var slideID = $(this).attr('slideID');


    if($(this).parent().find('.plus_sign').is(':visible'))
    {
      $(this).parent().find('.plus_sign').slideUp();
      $(this).parent().find('.minus_sign').slideDown();
      
      var windowWidth = parseInt($(window).width());
      var new_height = parseInt($('.header').css('height'));

      if(windowWidth < 769)
      {
        new_height = parseInt($('.navbar-header').css('height')); 
        var flag1 = $('.mobile_nav').is(':visible');
        var flag2 = $('.mobile_nav .navbar-collapse').is(':visible');

        if(flag1 && flag2)
        $('.navbar-toggle').trigger('click');
      }

      var post = $('#'+slideID);
      var position = post.position().top - $(window).scrollTop();
      $('html, body').stop().animate({
          'scrollTop': post.offset().top - new_height
      }, 900, 'swing', function () {
          
      });

      if(newsID == '1')
      {
        $(this).parent().find('.bot_cont_div').slideDown();
        $('.year_column') .css('height', h1);
        $('.year_column') .fadeIn();
      }

      else if(awardsID == '1')
      {
        $(this).parent().find('.bot_cont_div').slideDown();
        $('.year_column_2') .css('height', h2);
        $('.year_column_2') .fadeIn();
      }

      else $(this).parent().find('.bot_cont_div').slideDown();
    }

    else
    {
      $(this).parent().find('.plus_sign').slideDown();
      $(this).parent().find('.minus_sign').slideUp();
      

      $(this).parent().find('.bot_cont_div').slideUp();

      var newsID = $(this).attr('newsID');
      var awardsID = $(this).attr('awardsID');

      if(newsID == '1')
      $('.year_column').slideUp();

      if(awardsID == '1')
      $('.year_column_2').slideUp();
    }
    
// }
  });

  $('.news_more_btn').on('click',function (e) {

    e.preventDefault();

    $(this).hide();
    $(this).parent().find('.news_row_desc_2').show();

    var h1 = parseInt($('.news_row').css('height'));
    h1 = h1 - 50;
    h1 = h1 + 'px';
    $('.year_column') .css('height', h1);

  });

  $('.form_submit').on('submit',function (e) {

    
    e.preventDefault();
    return false;
    $('.send_spin').show();
    $('.pdf_submit_btn').hide();

    
    // return false;

    // var x = $(this).serialize();

    var formID = $(this).attr('formID');
    var inputs = $('.form_inputs');

    $.each( inputs, function( key, value ) {
      
      inputID = $(value).attr('inputID');
      var input_old = $(value).val();
      $('#input_' + formID + '_' + inputID).val(input_old);

    });

    var formID = $(this).attr('formID'); 

    var clone = $('#gform_' + formID).clone();
    $('#gform_' + formID).submit();
    $('.footer_left_div').append(clone);
    
    $('.md_trigger_sending').trigger('click');
    // return false;
    
    jQuery(document).bind('gform_post_render', function(){
   
    $('.md-close').trigger('click');

    setTimeout(function(){
      $('.md_trigger_success').trigger('click');
    }, 500);

    setTimeout(function(){
      $('.form_inputs').val('');
      $('.md-close').trigger('click');
    }, 1500);

    setTimeout(function(){


   }, 

     1500);
    
  });

   });

});

window.onload = function() {

var h1 = parseInt($('.news_row').css('height'));
h1 = h1 - 50;
h1 = h1 + 'px';
$('.year_column') .css('height', h1);

var h2 = parseInt($('.awards_row').css('height'));
h2 = h2 - 50;
h2 = h2 + 'px';
$('.year_column_2') .css('height', h2);

$('.main_container').hide();

$('#loading').fadeOut();
$('.main_container').fadeIn(750);

var windowWidth = parseInt($(window).width());

if(windowWidth < 769)
{
  $('.top_div').addClass('top_div_2');
}
else
{
  $('.top_div').removeClass('top_div_2');
}

}

$(window).on('resize', function()
{
  var windowWidth = parseInt($(window).width());
  if(windowWidth < 769)
  {
    $('.year_column') .css('top', '83px');
    $('.year_column_2') .css('top', '83px');
  }

  else
  {
    $('.year_column') .css('top', '90px'); 
    $('.year_column_2') .css('top', '90px'); 
  } 

  var h1 = parseInt($('.news_row').css('height'));
  h1 = h1 - 50;
  h1 = h1 + 'px';
  $('.year_column') .css('height', h1);

  var h2 = parseInt($('.awards_row').css('height'));
  h2 = h2 - 50;
  h2 = h2 + 'px';
  $('.year_column_2') .css('height', h2);

});

$(window).scroll(function() {

    var eTop = $('.main_container').offset().top;
    // console.log(eTop - $(window).scrollTop());
    if(parseInt(eTop - $(window).scrollTop()) < -55)
    {
      $('.header').addClass('header_2');
    }

    else
    {
      $('.header').removeClass('header_2');
    } 

  });

</script>