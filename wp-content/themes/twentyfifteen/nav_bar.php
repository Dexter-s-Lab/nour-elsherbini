<div class="header_div">
    <div class="logo_div">
		<a href="#home" class="nav_links">
  			<img src="assets/img/logo.png">
		</a>
    </div>
    <div class="nav_rest">
      <a href="#features" class="nav_links">
      	<span>features</span>
      </a>
      <a href="#models" class="nav_links">
      	<span>models</span>
      </a>
      <a href="#master_plan" class="nav_links">
      	<span>master plan</span>
      </a>
      <a href="#direction_map" class="nav_links">
      	<span>direction map</span>
      </a>
      <a href="#the_developers" class="nav_links">
      	<span>the developers</span>
      </a>
      <a href="#contacts" class="nav_links">
      	<span>contacts</span>
      </a>
      <a href="#" class="bro_btn">
        <img src="assets/img/download.png">
      </a>
    </div>
  </div>
  <div class="fake_head">
  </div>

  <nav class="navbar navbar-default navbar-fixed-top mobile_nav">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand mobile_links" href="#home" >
              <img src="assets/img/logo.png">
            </a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="#features" class="mobile_links">features</a></li>
              <li><a href="#models" class="mobile_links">models</a></li>
              <li><a href="#master_plan" class="mobile_links">master plan</a></li>
              <li><a href="#direction_map" class="mobile_links">direction map</a></li>
              <li><a href="#the_developers" class="mobile_links">the developers</a></li>
              <li><a href="#contacts" class="mobile_links">contacts</a></li>
              <li><a href="#contacts" class="bro_btn"><img src="assets/img/download.png"></a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>